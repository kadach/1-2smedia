(function ($) {
    var input = $('input');
    var inputText = $('textarea');
    var videoBtn = $('.video-wrap');


    $(window).on('load', function () {
        var preload = $('.prelod');
        preload.addClass('close');
        setTimeout(function () {
            preload.addClass('die');
        },610)

    });


    $('[data-scroll-go]').click(function () {
        $('html, body').animate({ scrollTop: $($(this).attr('data-scroll-go')).offset().top }, 500);
    });


    //EVENT FOR SCROLL ***********************************************
    var dataScroll = $('[data-scroll="off"]');
    $(window).on('load', function () {
        var top = $(window).scrollTop() + ($(window).height() * 0.9);
        for (var i = 0; i < dataScroll.length; i++) {
            if ($(dataScroll[i]).offset().top < top && $(dataScroll[i]).attr('data-scroll') == 'off') {
                $(dataScroll[i]).attr('data-scroll', "on");
            }
        }
        $(window).on('scroll', function () {
            var top = $(window).scrollTop() + ($(window).height() * 0.9);
            for (var i = 0; i < dataScroll.length; i++) {
                if ($(dataScroll[i]).offset().top < top && $(dataScroll[i]).attr('data-scroll') == 'off') {
                    $(dataScroll[i]).attr('data-scroll', "on");
                    dataScroll = $('[data-scroll="off"]');
                }
            }
        });
    });
//FOR THANK POPUP ***************************************************
    $(window).on('load', function () {
        if($(window).width()>768){
            $('.thank').click(function () {
                var thankImg = $(this).children('img').attr('src');
                var thankPopup = $('.thank-popup');
                var thankPopupImg = $('.thank-popup>img');
                thankPopupImg.attr('src',thankImg);

                thankPopupImg.click(function (e) {
                    e.preventDefault();
                    e.stopPropagation();
                });

                thankPopup.addClass('open');
                setTimeout(function () {
                    thankPopup.addClass('active');
                }, 1);


                thankPopup.click(function () {
                    thankPopup.removeClass('active');
                    setTimeout(function () {
                        thankPopup.removeClass('open');
                    }, 500);
                });

                var closeVideo = $('.close-video');
                closeVideo.click(function () {
                    thankPopup.click(function () {
                        thankPopup.removeClass('active');
                        setTimeout(function () {
                            thankPopup.removeClass('open');
                        }, 500);
                    });
                });
            });
        } else {
            var blabla = $('.video-wrap');
            $(blabla[3]).addClass('closed die');
            $(blabla[4]).addClass('closed die');
            $(blabla[5]).addClass('closed die');
        }
    });

    //SORT FOR ALL_SERVICE_PAGE
    var sortBtn = $('[data-sort2]');
    var allCategory = $('.category');
    var allCategoryWrap = $('.all-category');
    $(window).load(function () {
        allCategoryWrap.css('height', $(allCategory[0]).height());
    });
    sortBtn.click(function () {
        var thisSort = $($(this).attr('data-sort2')+"");
        allCategoryWrap.css('height', thisSort.height());
        sortBtn.removeClass('active');
        $(this).addClass('active');
        allCategory.addClass('closed');
        setTimeout(function () {
            allCategory.addClass('die');
            thisSort.removeClass('die');
            setTimeout(function () {
                thisSort.removeClass('closed');
            },30);
        },530);

    });


    //CLICK ON FILTER PORTFOLIO
    var filterBtn = $('[data-sort]'),
        allExamples = videoBtn,
        addBtn = $('[add-exapmple]');
    filterBtn.on('click', function () {
        var search = $(this).attr('data-sort') + "",
            searchArr = $(search);

        filterBtn.removeClass('active');
        $(this).addClass('active');

        allExamples.addClass('closed');
        setTimeout(function () {
            allExamples.addClass('die')
        }, 500);
        if (searchArr.length > 6) {
            addBtn.removeClass('die');
            addBtn.attr('add-exapmple', search);
            setTimeout(function () {
                searchArr.slice(0, 6).removeClass('die');
                setTimeout(function () {
                    searchArr.slice(0, 6).removeClass('closed');
                }, 100);

            }, 501);
        } else {
            addBtn.addClass('die');
            setTimeout(function () {
                searchArr.removeClass('die');
                setTimeout(function () {
                    searchArr.removeClass('closed');
                }, 100);
            }, 501);
        }
    });
    addBtn.on('click', function () {
        var search = $(this).attr('add-exapmple') + ".closed",
            searchArr = $(search);
        if (searchArr.length > 6) {
            setTimeout(function () {
                searchArr.slice(0, 6).removeClass('die');
                setTimeout(function () {
                    searchArr.slice(0, 6).removeClass('closed');
                }, 100);
            }, 501);
        } else {
            addBtn.addClass('die');
            searchArr.removeClass('die');
            setTimeout(function () {
                searchArr.removeClass('closed');
            }, 100);
        }
    });


    //NAV MENU *******************************************************
    var navBtn = $('.nav-btn');
    var navMenu = $('.nav-menu-wrap');
    var navMenuThis = $('.nav-menu');
    navBtn.click(function () {
        if (!$(this).hasClass('active')) {

            navBtn.addClass('active');
            navMenu.addClass('open');
            setTimeout(function () {
                navMenu.addClass('active');
            }, 1);

            navMenu.click(function () {
                if($(event.target).hasClass('container')){
                    navBtn.removeClass('active');
                    navMenu.removeClass('active');
                    setTimeout(function () {
                        navMenu.removeClass('open');
                    }, 500);
                }
            })
        } else {
            navBtn.removeClass('active');
            navMenu.removeClass('active');
            setTimeout(function () {
                navMenu.removeClass('open');
            }, 500);
        }
    });

    //VIDEO POPUP
    var iframeWrap = $('.video-popup');
    var iframeUrl = $('.video-popup > iframe');
    videoBtn.click(function () {
        iframeUrl.css('height', (iframeUrl.width() / 16 * 9) + "px");
        iframeUrl.attr('src', $(this).children('.video-url').text());
        iframeWrap.addClass('open');
        setTimeout(function () {
            iframeWrap.addClass('active');
        }, 1);


        iframeWrap.click(function () {
            iframeUrl.attr('src', "");
            iframeWrap.removeClass('active');

            setTimeout(function () {
                iframeWrap.removeClass('open');
            }, 500);
        });

        var closeVideo = $('.close-video');
        closeVideo.click(function () {
            iframeUrl.attr('src', "");
            iframeWrap.click(function () {
                iframeWrap.removeClass('active');
                setTimeout(function () {
                    iframeWrap.removeClass('open');
                }, 500);
            });
        });
    });

    //PoPup for call *************************************************
    var btn = $('[data-call]');
    var callClose = $('.close');
    btn.click(function () {

        var callOpen = $($(this).attr('data-call'));
        var callForm = callOpen.children('form');

        //animate open
        callOpen.addClass('open');
        setTimeout(function () {
            callOpen.addClass('active');
        }, 1);

        //animate exit
        callClose.click(function () {
            callOpen.removeClass('active');
            setTimeout(function () {
                callOpen.removeClass('open');
            }, 500);
        });
        callOpen.click(function () {
            callOpen.removeClass('active');
            setTimeout(function () {
                callOpen.removeClass('open');
            }, 500);
        });

        //cancel event in parent form
        callForm.click(function (e) {
            e.preventDefault();
            e.stopPropagation();
        });

        //cancel window scroll in open call window
        // var winScrollTop = $(window).scrollTop();
        // $(window).bind('scroll', function () {
        //     $(window).scrollTop(winScrollTop);
        // });
        // $(window).unbind('scroll');
    });

    //mail ***********************************************************
    var mailBtn = $('[data-btn]'),
        validUserName = "",
        validUserTell = "",
        validUserMassage = "",
        boolName = false,
        boolTell = false;
    inputText.blur(function () {
        validUserMassage = $(this).val()
    });
    input.blur(function () {
        var userName = /^[а-яА-ЯёЁa-zA-Z][а-яА-ЯёЁa-zA-Z\s]+$/;
        // var userEmail = /^[-\w.]+@([A-z0-9][-A-z0-9]+\.)+[A-z]{2,4}$/;
        var userTell = /^((\d|\+\d)[\- ]?)?(\(?\d{3}\)?[\- ]?)?[\d\- ]{6,13}$/;
        var inType = $(this).attr('name');
        var inVal = $(this).val();
        var inputWrap = $(this).closest('.input-wrap');
        if (inType == 'name') {
            if (inVal.length > 1 && userName.test(inVal)) {
                inputWrap.removeClass('error');
                boolName = true;
                validUserName = inVal;
            } else {
                inputWrap.addClass('error');
                boolName = false;
            }
        }
        // if (inType == 'email') {
        //     if (inVal.length > 1 && userEmail.test(inVal)) {
        //     inputWrap.removeClass('error');
        //     boolEmail = true;
        //     validUserEmail = inVal;
        //     } else {
        //     inputWrap.addClass('error');
        //     }
        // }
        if (inType == 'tell') {
            if (userTell.test(inVal) && inVal.length > 6) {
                inputWrap.removeClass('error');
                boolTell = true;
                validUserTell = inVal;
            } else {
                inputWrap.addClass('error');
                boolTell = false;
            }
        }
    });
    mailBtn.on('click', function () {
        var allInput = $(this).closest('form');
        var inputWrap = allInput.children('.input-wrap');
        var thisBtn = $(this);

        if (boolName && boolTell) {

            var data1 = {name: validUserName, tell: validUserTell,massage : validUserMassage};
            $.ajax({
                // url: "../sender.php",
                type: "POST",
                data: data1,
                success: function () {
                    thisBtn.addClass('die');
                    setTimeout(function () {
                        thisBtn.children('span').text('ОТПРАВЛЕНО!');
                        thisBtn.removeClass('die');
                        thisBtn.unbind('click');
                    }, 500);
                    // thisBtn.children('span').text('отправлено');
                    // thisBtn.next('.success').addClass('active');
                }
            });
        } else {
            if (!boolName) {
                $(inputWrap[0]).addClass('error');
            }
            if (!boolTell) {
                $(inputWrap[1]).addClass('error');
            }
        }
    });


    //hover for input
    input.focus(function () {
        var thisInput = $(this).parent('.input-wrap');
        thisInput.addClass('focus');
    });
    input.blur(function () {
        $(this).parent('.input-wrap').removeClass('focus');
    });


    //FOR CALCULATOR
    var calculator = {
        allBtn: $('.answer'),
        parentBtn: "",
        nextParentBtn: "",
        nextParentBtnHeight: "",
        allAnswer: $('.all-answer'),
        question: "",
        nextQuestion: "",
        priseBtn: 0,
        maxPrice: 0,
        allPrice: 0,
        textPrice: $('.text-price'),
        priseClass: "",
        percent: $('.result:first-child').height() / 100,
        allResultText: $('.result-text'),
        resetBtn: $('.clear'),
        answerWrapFirst: $('.answer-wrap:first-child'),
        answerWrap: $('.answer-wrap')
    };
    $(window).on('load', function () {
        calculator.allAnswer.css('height', calculator.allAnswer.height() + "px");
    });
    calculator.allBtn.click(function () {

        calculator.parentBtn = $(this).parent();//родитель кнопки на которую нажали
        calculator.priseClass = $($(this).attr('data-result')).children('.result-move');
        calculator.priseTextClass = $($(this).attr('data-result')).children('.result-text');
        calculator.priseBtn = $(this).attr('data-price') * 1;
        calculator.question = $('.question.active');
        calculator.nextParentBtn = calculator.parentBtn.next();
        calculator.nextParentBtnHeight = calculator.nextParentBtn.height() || 0;
        calculator.nextQuestion = calculator.question.next();
        calculator.allPrice += calculator.priseBtn;
        calculator.textPrice.text(calculator.allPrice + " руб.");

        calculator.question.removeClass('active');
        calculator.parentBtn.addClass('close');
        calculator.allAnswer.css('height', calculator.nextParentBtnHeight + "px");
        setTimeout(function () {
            calculator.nextQuestion.addClass('active');
            calculator.parentBtn.addClass('die');
            calculator.nextParentBtn.removeClass('die');
            setTimeout(function () {
                calculator.nextParentBtn.removeClass('close');
            }, 30);
        }, 401);

        if (calculator.maxPrice <= calculator.priseBtn) {
            calculator.maxPrice = calculator.priseBtn;
            calculator.priseClass.css('height', calculator.percent * 100 + "px");
            calculator.priseTextClass.text(calculator.priseBtn);
            calculator.priseTextClass.addClass('active');
            calculator.maxPricePercent = calculator.maxPrice / 100;
            for (var i = 0; i < calculator.allResultText.length; i++) {
                var prise = $(calculator.allResultText[i]).text() * 1;
                var resultMove = $(calculator.allResultText[i]).prev('.result-move');
                resultMove.css('height', prise / calculator.maxPricePercent * calculator.percent + "px");
            }
        } else {
            calculator.maxPricePercent = calculator.maxPrice / 100;
            calculator.priseClass.css('height', calculator.priseBtn / calculator.maxPricePercent * calculator.percent + "px");
            calculator.priseTextClass.text(calculator.priseBtn);
            calculator.priseTextClass.addClass('active');
        }

        calculator.resetBtn.click(function () {
            calculator.maxPrice = 0;
            calculator.allPrice = 0;
            calculator.allResultText.removeClass('active');
            calculator.answerWrapFirst.removeClass('close');
            $('.result-move').css('height', "0px");
            $('.question').removeClass('active');
            calculator.answerWrap.addClass('close');
            calculator.allAnswer.css('height', calculator.answerWrapFirst.height() + "px");
            setTimeout(function () {
                calculator.allResultText.text("");
                $('.question:first-child').addClass('active');
                calculator.answerWrap.addClass('die');
                calculator.answerWrapFirst.removeClass('die');
                setTimeout(function () {
                    calculator.answerWrapFirst.removeClass('close')
                },30);
            }, 500);
        });
    });


})(jQuery);